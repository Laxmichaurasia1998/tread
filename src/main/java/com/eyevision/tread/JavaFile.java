package com.eyevision.tread;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public class JavaFile {
	public void readDataFromFile() throws IOException {
		//File PAth
		String file29Jan = "C:\\git\\tread\\src\\main\\resources\\csvFile\\input\\cm29JAN2020bhav.csv";
		String file30Jan = "C:\\git\\tread\\src\\main\\resources\\csvFile\\input\\cm30JAN2020bhav.csv";
		String file31Jan = "C:\\git\\tread\\src\\main\\resources\\csvFile\\input\\cm31JAN2020bhav.csv";
		String fileOutPut = "C:\\git\\tread\\src\\main\\resources\\csvFile\\output\\cmOutPut"+new Date().getTime()+".csv";
		//Convert file data to object
		List<TreadInfo> treadData29Jan = getDataForFile(file29Jan);
		List<TreadInfo> treadData30Jan = getDataForFile(file30Jan);
		List<TreadInfo> treadData31Jan = getDataForFile(file31Jan);
		List<TreadInfo> treadData = new ArrayList<TreadInfo>();
		treadData.addAll(treadData29Jan);
		treadData.addAll(treadData30Jan);
		treadData.addAll(treadData31Jan);
		System.out.print("treadData size : " + treadData.size());

		Collections.sort(treadData);
		getWriteDatatoFile(treadData, fileOutPut);

		System.out.println("data PROCESS is done plz check the file for data :   " + fileOutPut);
	}

	private List<TreadInfo> getDataForFile(String fileName) {
		BufferedReader br;
		List<TreadInfo> treadData = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
			treadData = br.lines().skip(1).map(line -> {
				return convertStringToTreadInfo(line);
			}).collect(Collectors.toList());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return treadData;
	}

	private void getWriteDatatoFile(List<TreadInfo> treadInfoList, String filePath) {

		File file = new File(filePath);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("SYMBOL,SERIES,OPEN,HIGH,LOW,CLOSE,LAST,PREVCLOSE,TOTTRDQTY,TOTTRDVAL,TIMESTAMP,TOTALTRADES,ISIN,RANGE,AVERAGE\n");
			
			for (TreadInfo treadInfo : treadInfoList) {
				bw.write(convertTreadInfoToString(treadInfo)+"\n");
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
//SYMBOL	SERIES	OPEN	HIGH	LOW	CLOSE	LAST	PREVCLOSE	TOTTRDQTY	TOTTRDVAL	TIMESTAMP	TOTALTRADES	ISIN	RANGE	AVERAGE

	private String convertTreadInfoToString(TreadInfo t) {
		String s = t.getSymbol() + "," + t.getSeries() + "," + t.getOpen() + "," + t.getHigh() + "," + t.getLow() + ","
				+ t.getClose() + "," + t.getLast() + "," + t.getPrevClose() + "," + t.getTottrdQty() + ","
				+ t.getTottrddVl() + "," + t.getTimeStamp() + "," + t.getTotalTreades() +"," + t.getIsin() + ","
				+ t.getRange() + "," + t.getAverage();
		return s;
	}

	private TreadInfo convertStringToTreadInfo(String st) {
		String[] data = st.split(",");
		TreadInfo t = new TreadInfo();
		t.setSymbol(data[0]);
		t.setSeries(data[1]);
		t.setOpen(Float.parseFloat(data[2]));
		t.setHigh(Float.parseFloat(data[3]));
		t.setLow(Float.parseFloat(data[4]));
		t.setClose(Float.parseFloat(data[5]));
		t.setLast(Float.parseFloat(data[6]));
		t.setPrevClose(Double.parseDouble(data[7]));
		t.setTottrdQty(Double.parseDouble(data[8]));
		t.setTottrddVl(Double.parseDouble(data[9]));
		Date date1;
		try {
			date1 = new SimpleDateFormat("dd-MMM-yyyy").parse(data[10]);
			t.setTimeStamp(date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		t.setTotalTreades(Float.parseFloat(data[11]));
		t.setIsin(data[12]);
		t.setRange(t.getHigh() - t.getLow());
		t.setAverage((t.getHigh() + t.getLow()) / 2);
		return t;

	}
}
