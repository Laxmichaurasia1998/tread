package com.eyevision.tread;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class TreadApplication {
	final Logger LOGGER = LoggerFactory.getLogger(TreadApplication.class);

	public static void main(String[] args) throws IOException {
		SpringApplication.run(TreadApplication.class, args);
		new TreadApplication().setUp();
		
		
	}

	// @Autowired
	// private JavaFile javaFile;

	public void setUp() throws IOException {
		new JavaFile().readDataFromFile();
		LOGGER.info("application is run");
	}
}
