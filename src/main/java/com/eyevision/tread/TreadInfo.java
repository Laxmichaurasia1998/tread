package com.eyevision.tread;

import java.util.Date;


public class TreadInfo implements Comparable< TreadInfo > {
	String symbol;
	String series;
	float open;
	float high;
	float low;
	float close;
	float last;
	double prevClose;
	double tottrdQty;
	double tottrddVl;
	Date timeStamp;
	float totalTreades;
	String isin;
float range;
float average ;
	public float getAverage() {
	return average;
}

public void setAverage(float average) {
	this.average = average;
}

	public TreadInfo() {
		super();
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public float getOpen() {
		return open;
	}

	public void setOpen(float open) {
		this.open = open;
	}

	public float getHigh() {
		return high;
	}

	public void setHigh(float high) {
		this.high = high;
	}

	public float getLow() {
		return low;
	}

	public void setLow(float low) {
		this.low = low;
	}

	public float getClose() {
		return close;
	}

	public void setClose(float close) {
		this.close = close;
	}

	public float getLast() {
		return last;
	}

	public void setLast(float last) {
		this.last = last;
	}

	public double getPrevClose() {
		return prevClose;
	}

	public void setPrevClose(double prevClose) {
		this.prevClose = prevClose;
	}

	public double getTottrdQty() {
		return tottrdQty;
	}

	public void setTottrdQty(double tottrdQty) {
		this.tottrdQty = tottrdQty;
	}

	public double getTottrddVl() {
		return tottrddVl;
	}

	public void setTottrddVl(double tottrddVl) {
		this.tottrddVl = tottrddVl;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public float getTotalTreades() {
		return totalTreades;
	}

	public void setTotalTreades(float totalTreades) {
		this.totalTreades = totalTreades;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public TreadInfo(String symbol, String series, float open, float high, float low, float close, float last,
			double prevClose, double tottrdQty, double tottrddVl, Date timeStamp, float totalTreades, String isin) {
		super();
		this.symbol = symbol;
		this.series = series;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.last = last;
		this.prevClose = prevClose;
		this.tottrdQty = tottrdQty;
		this.tottrddVl = tottrddVl;
		this.timeStamp = timeStamp;
		this.totalTreades = totalTreades;
		this.isin = isin;
	}
	

	public float getRange() {
		return range;
	}

	public void setRange(float range) {
		this.range = range;
	}

	@Override
	public String toString() {
		return "TreadInfo [symbol=" + symbol + ", series=" + series + ", open=" + open + ", high=" + high + ", low="
				+ low + ", close=" + close + ", last=" + last + ", prevClose=" + prevClose + ", tottrdQty=" + tottrdQty
				+ ", tottrddVl=" + tottrddVl + ", timeStamp=" + timeStamp + ", totalTreades=" + totalTreades + ", isin="
				+ isin + "]";
	}
	
	 @Override
	    public int compareTo(TreadInfo o) {
	        return this.getSymbol().compareTo(o.getSymbol());
	    }
}
